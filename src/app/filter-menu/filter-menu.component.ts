import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filter-menu',
  templateUrl: './filter-menu.component.html',
  styleUrls: ['./filter-menu.component.css']
})
export class FilterMenuComponent implements OnInit {

  public nameFilter: NameFilter;
  public JSON: Object = JSON;

  constructor() { }

  ngOnInit(): void {
  }

  filterMenus()

}

export interface NameFilter {
  name: String;
}
