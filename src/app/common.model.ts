export class Menu implements MenuInterface {
    public id?: number;
    public name: String;
    public price: number;
    public category: CategoryInterface;

    constructor(menuCfg: MenuInterface){
        this.id = menuCfg.id;
        this.name = menuCfg.name;
        this.price = menuCfg.price;
        this.category = menuCfg.category;
    }
}

interface MenuInterface{
    id?: number;
    name: String;
    price: number;
    category: CategoryInterface;
}

export interface CategoryInterface{
    id?: number;
    name?: String;
}