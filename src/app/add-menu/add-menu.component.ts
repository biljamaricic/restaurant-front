import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../app.service';
import { CategoryInterface, Menu } from '../common.model';

@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.css']
})
export class AddMenuComponent implements OnInit {
  public newMenu: Menu;
  public categories: CategoryInterface[] = [];

  constructor(private appService: AppService, private router: Router) {
    this.newMenu = new Menu ({
      name: '',
      price: 0,
      category: {}
    });
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.appService.getCategories().subscribe((res: CategoryInterface[]) => {
      this.categories = res;
    })
  }

  addMenu() {
    this.appService.addMenu(this.newMenu).subscribe((res: Menu) => {
      this.newMenu = res;
      this.router.navigate(['/menus']);
    });
  }

  resetMenu(){
    this.newMenu = {
      name: '',
      price: 0,
      category: {}
    }
  }
}
