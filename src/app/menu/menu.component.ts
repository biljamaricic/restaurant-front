import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Menu } from '../common.model';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  @Input() menu: Menu;
  @Input() index: number = -1;
  @Output() deleteMenuIndex:EventEmitter<number> = new EventEmitter();

  constructor() {
    this.menu = new Menu ({
      name: '',
      price: 0,
      category:{}
    });
  }

  ngOnInit(): void {
  }

  deleteMenu(id: number){
    this.deleteMenuIndex.emit(id);
  }

}
