import { Component, Input, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Menu } from '../common.model';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent implements OnInit {
  @Input() menus: Menu[];

  constructor(private appService: AppService) {
    this.menus =[];
  }

  ngOnInit(): void {
    this.getMenus();
  }

  getMenus(){
    this.appService
      .getMenus()
      .subscribe((res: Menu[]) => (this.menus = res));
  }

  delete(id: number){
    this.appService.deleteMenu(id).subscribe((res: Menu)=> {this.getMenus()});
  }
  
}
