import { Injectable } from '@angular/core';
import { catchError, Observable, of, Subject } from 'rxjs';
import { CategoryInterface, Menu } from './common.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private menusUrl = 'http://localhost:8080/api/menus';
  private categoriesUrl = 'http://localhost:8080/api/categories';

  menuEdit$: Observable<Menu>;
  private menuSubject: Subject<Menu>;
  
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) {
    this.menuSubject = new Subject<Menu>();
    this.menuEdit$ = this.menuSubject.asObservable();
  }

  getMenu(id: number): Observable<Menu>{
    const url = `${this.menusUrl}/${id}`;

    return this.http
    .get<Menu>(url)
    .pipe(catchError(this.handleError<Menu>('getmenu')));
  }
  
  getMenus(): Observable<Menu[]> {
    return this.http
      .get<Menu[]>(
      `${this.menusUrl}`
    )
      .pipe(catchError(this.handleError<Menu[]>('getMenus', [])));
  }

  getCategories(): Observable<CategoryInterface[]> {
    return this.http
      .get<CategoryInterface[]>(`${this.categoriesUrl}`)
      .pipe(catchError(this.handleError<CategoryInterface[]>('getCategories', [])));
  }

  /** POST: add a new Menu to the server */
  addMenu(menu: Menu): Observable<Menu>{
    return this.http
      .post<Menu>(this.menusUrl, menu, this.httpOptions)
      .pipe(catchError(this.handleError<Menu>('addRecord')))
  }

  /** DELETE: delete the Menu from the server */
  deleteMenu(id: number): Observable<Menu> {
    const url = `${this.menusUrl}/${id}`;

    return this.http
      .delete<Menu>(url, this.httpOptions)
      .pipe(catchError(this.handleError<Menu>('deleteMenu')))
  }
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
   private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
